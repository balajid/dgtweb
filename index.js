var express=require('express');
var app = express();
var path=require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'public')));
var jobs=[{'item1':'GAGER','job':65343},{'item1':'GAGER','job':68972},{'item1':'GAGER','job':15746}];
var cure=[{'item1':'SPIRIT','name':'SPEC 1 300'},{'item1':'BAC','name':'SPEC 2 100'},{'item1':'GAGER','name':'SPEC 300'}];
var location=[{'part':1177,'loc':'Bay1'},{'part':1292,'loc':'Bay2'},{'part':1397,'loc':'Bay3'},{'part':1424,'loc':'Bay4'},{'part':1662,'loc':'Bay5'}]
app.get('/',function(req,res){
    //res.render('start');
    var customer=[{'item1':'SPIRIT'},{'item1':'BAC'},{'item1':'GAGER'}];
    var equipment=[{'part':1177},{'part':1292},{'part':1397},{'part':1424},{'part':1662}];
    //var jobs=[{'item1':'GAGER','job':65343},{'item1':'GAGER','job':68972},{'item1':'GAGER','job':15746}]
    //var cure=[{'item1':'SPIRIT','name':'SPEC 1 300'},{'item1':'BAC','name':'SPEC 2 100'},{'item1':'GAGER','name':'SPEC 300'}]
    var color=[{'job':65343,'code':'#A52A2A'},{'job':68972,'code':'#B0C4DE'},{'job':15746,'code':'#BA55D3'}]
    var load=[{'name':'ABC','load':60001},{'name':'CDE','load':60002}]
    var operators=[{'operator':873},{'operator':577},{'operator':1255}]

    //console.log(req.params);
    //res.redirect('/');
    res.render('index',{cparts:customer,equip:equipment,jobs:jobs,cure:cure,color:color,loads:load,operators:operators,location:location});
});

app.post('/jobgetter',function(req,res){

    var parts=req.body.text;
    console.log(req.body.text);
    var jobObject2=[];
    jobs.forEach(function(jp){
        var l=jp.item1;
        console.log(l);
        console.log(parts);
        if(l==parts){

          jobObject2.push(jp.job);
        }

    });
    res.send(jobObject2);

});

app.post('/curegetter',function(req,res){

    var parts=req.body.text;
    var cureObject2=[];
    cure.forEach(function(cd){
      var ts=cd.item1;
      if(ts==parts){
          cureObject2.push(cd.name);

      }
    });
    res.send(cureObject2);

});

app.post('/checker',function(req,res){

      var equips=req.body.text;
      var loca;
      location.forEach(function(loc){
          var te=loc.part;
          if(te==equips)
          {
               loca=loc.loc;
          }
      });
      res.send(loca);

});
app.post('/node/add',function(req,res){

      const tc1=req.body.myArray;
      const tc2=req.body.cparts;
      const tc3=req.body.equip;
      const tc4=req.body.curespec;
      const date=req.body.dater2;
      const location=req.body.locat;
      console.log(tc1);
      console.log(tc2);
      console.log(tc3);
      console.log(tc4);
      console.log(date);
      console.log(location);
      res.redirect('/');
      //const ec1=req.body.ec;
      //const sc1=req.body.sc;
      //const vc1=ec1.concat(sc1);
      //var operator=req.body.operator;
      //var ops=operator.split("/");
      //console.log(ops);
      //res.render('index2',{tc:tc1,operator:ops,vcs:vc1});


});

app.listen(3000);

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');
